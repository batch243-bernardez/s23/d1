// [Section] Objects
/*
	- An object is a data type that is used to represent real world objects.
	- It is a collection of related data and/or functionalities or method.
	- Information stored in objects are represented in a "key:value" pair
	- Key is mostly referred to as "property" of an object
	- Different data type may be stored in an object's property creating data structures.
*/


// Creating objects using object initializer/literal notation (keyA)
	/*
		- This creates or declares an object and initializes or assign its properties upon creation.
		- A cellphone is an example of real world object. It has its own properties such as name, color, unit model, weight, etc.

		Syntax:
			let objectName = {
				keyA: valueA;
				keyB: valueB;
			}
	*/

	/*Example*/
	let cellphone = {
		name: "Nokia 3210",
		manufactureData: 1999,
	}

	console.log("Result from creating using LITERAL NOTATION:");
	console.log(cellphone);


// Creating objects using constructor function (this.keyA) (semicolon)
	/*
		- Creates a reusable function to create several objects that have the same data structure
		- This is useful for creating multiple instances or copies of an object
		- An instance is a concrete occurence of any object which emphasize distinct or unique identity of it (instatiation)

		Syntax:
			function objectName(valueA, valueB){
				this.keyA = valueA;
				this.keyB = valueB;
			}
	*/

	function Laptop(name, manufactureDate, ram){
		this.laptopName = name;
		this.laptopManufactureDate = manufactureDate;
		this.laptopRam = ram;
	}
	// "this" keyword allows us to assign a new object's properties by associating them with values that was received from a constructor function's parameter.

	// Instatiation
	// The "new" operator creates an instances of an object
	// Objects and instances are often interchange because object literals(let object = {}) and instances(let objectName = new functionName(arguments)) are distinct or unique objects

	/*Example*/
	let laptop = new Laptop('Lenovo', 2008, "4 gb");
	console.log("Result from creating objects using OBJECT CONSTRUCTOR:");
	console.log(laptop);

	/*Example*/
	let myLaptop = new Laptop('Macbook Air', 2020, "8 gb");
	console.log("Result from creating objects using OBJECT CONSTRUCTOR:");
	console.log(myLaptop);

	/*Example*/
	/*This example invoke or calls the laptop function instead of creating a new object. It will return "undefined" without the "new" operator because the "laptop" function does not have any return statement*/
	let oldLaptop = Laptop("Portal R2e CCMC", 1980, "500 mb");
	console.log("Result from creating objects without the new keyword:");
	console.log(oldLaptop);
	/*ans: undefined*/


/*----------MINI ACTIVITY----------*/
// Create a constructor function that will let us instatiate a new object, menu, property: menuName, menuPrice
	
	function Menu(name, price){
		this.menuName = name;
		this.menuPrice = price;
	}

	let menu1 = new Menu('Chicken', 199);
	console.log(menu1);

	let menu2 = new Menu('Sushi', 150);
	console.log(menu2);
/*----------MINI ACTIVITY----------*/


// Creating Empty Objects (comma)
	let computer = {};
	let myComputer = new Object();
	console.log(computer); /*ans: {} <-- empty object*/
	console.log(myComputer); /*ans: {} <-- empty object*/


// Accessing Specific Objects inside an Array
	let array = [laptop, myLaptop];
	console.log(array);
	console.log(array[0]['laptopName']); /*ans: Lenovo*/

	// Don notation
	console.log(array[0].laptopManufactureDate); /*ans: 2008*/

	// in laptop
	console.log(laptop);
	console.log(laptop.laptopRam); /*ans: 4gb*/



// [Section] Initializing/Adding/Deleting/Reassigning Object Properties
	/*
		- Like any other variable in JavaScript, objects have their properties initialized or added after the object was created or declared.
		- Case sensitive (lower and uppercase)
	*/

	let car = {};
	console.log(car);

// INITIALIZING/ADDING object properties using DOT notation
	car.name = 'Honda Civic';
	console.log(car);

// INITIALIZING/ADDING object property using the BRACKET notation
	car['manufactureDate'] = '2019';
	console.log(car);

// DELETING object properties using BRACKET notation
	/*delete car['name'];
	console.log(car);*/

// DELETING object properties using DOT notation
	delete car.manufactureDate;
	console.log(car);

// REASSIGNING object properties using DOT notation
	car.name = "Dodge Charger R/T";
	console.log(car);

// REASSIGNING object properties using BRACKET notation
	car['name'] = "Jeepney";
	console.log(car);



// [Section] Object Methods
// A method is a function which is a property of an object
// They are also functions and one of the key differences they have is that methods are functions related to a specific object

	let person = {
		name: 'John',
		talk: function(){
			console.log("Hello my name is " + this.name)
		}
	}

	console.log(person); /*ans: {name: 'John', talk: ƒ}*/
	person.talk(); /*ans: Hello my name is John*/


	// Add Method to Objects
	person.walk = function(){
		console.log(this.name + " walked 25 steps forward");
	}

	person.walk(); /*ans: John walked 25 steps forward*/

	// Methods are useful for creating reusable functions that perform tasks related to objects
	let friends = {
		firstName: 'Joe',
		lastName: 'Smith',
		address: {
			city: 'Austin',
			state: 'Texas',
		},
		phoneNumber: [['09167343203'],['09123456789']],
		emails: ['joe@mail.com', 'joesmith@mail.xyz'],
		introduce: function(){
			console.log("Hello my name is " + this.firstName + " " + this.lastName + ". I live in " + this.address.city + ", " + this.address.state + ". My emails are " + this.emails[0] + " and " + this.emails[1] + ". My numbers are " + this.phoneNumber[0][0] + " and " + this.phoneNumber[1][0]);
		}
	}

	friends.introduce(); /*ans: Hello my name is Joe Smith. I live in Austin, Texas. My emails are joe@mail.com and joesmith@mail.xyz. My numbers are 09167343203 and 09123456789*/


// Create an object constructor

function Pokemon(name, level){
	/*Properties of Pokemon*/
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth = 2 * level;
	this.pokemonAttack = level;

	/*Add method named tackle*/
	this.tackle = function(targetPokemon){
		console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);
		console.log("targetPokemon's health is now reduced to targetpokemonHealth")
	}

	this.fainted = function(){
		console.log(this.pokemonName + " fainted!");
	}
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let gyarados = new Pokemon("Gyarados", 20);
console.log(gyarados);

pikachu.tackle(gyarados);

gyarados.fainted();